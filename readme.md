# Description

An R package. A thin wrapper around the pygmail2 package on PyPI.

# Install

    require(devtools)
    install_bitbucket("kindlychung/rgmail")
